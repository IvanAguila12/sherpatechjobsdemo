package aguila.ivan.sherpajob.demo.ui.screens.splash;

import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import aguila.ivan.sherpajob.demo.R;
import aguila.ivan.sherpajob.demo.ui.base.BaseFragment;
import aguila.ivan.sherpajob.demo.ui.screens.main.MainActivity;

/**
 * Created by ivan on 15/7/17.
 */

public class SplashFragment extends BaseFragment implements SplashContract.View {

    private AppCompatTextView text;
    private RelativeLayout container;

    public static SplashFragment newInstance() {
        return new SplashFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_splash;
    }

    @Override
    protected void configView() {
        text = $(R.id.splash_text);
        container = $(R.id.splash_text_container);
    }

    @Override
    public void setPresenter(SplashContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showText(String text) {
        this.text.setText(text);
    }

    @Override
    public void animateText() {
        text.animate().translationXBy(container.getWidth() / 2 - text.getX() - text.getWidth() / 2);
        text.animate().translationYBy(container.getHeight() / 2 - text.getY() - text.getHeight() / 2);
    }

    @Override
    public void openApp() {
        startActivity(MainActivity.makeIntent(getBaseActivity()));
        getBaseActivity().finish();
    }
}
