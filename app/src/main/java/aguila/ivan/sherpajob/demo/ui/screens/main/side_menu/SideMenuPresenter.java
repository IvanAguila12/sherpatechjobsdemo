package aguila.ivan.sherpajob.demo.ui.screens.main.side_menu;

import android.text.TextUtils;

import org.w3c.dom.Text;

import aguila.ivan.sherpajob.demo.R;
import aguila.ivan.sherpajob.demo.api.APIService;
import aguila.ivan.sherpajob.demo.controllers.CityController;
import aguila.ivan.sherpajob.demo.controllers.UserController;
import aguila.ivan.sherpajob.demo.ui.base.BaseActivity;
import bolts.Continuation;
import bolts.Task;

/**
 * Created by ivan on 17/7/17.
 */

public class SideMenuPresenter implements SideMenuContract.Presenter {

    private BaseActivity activity;
    private SideMenuContract.View view;

    public SideMenuPresenter(BaseActivity activity, SideMenuContract.View view) {
        this.activity = activity;
        this.view = view;
    }

    @Override
    public void start() {

    }

    @Override
    public void searchCityForPostCode(final String username, final String postCode) {
        if (!checkField(username)) {
            activity.showErrorMessage(activity.getString(R.string.side_menu_username_error));
            return;
        }
        if (!checkField(postCode)) {
            activity.showErrorMessage(activity.getString(R.string.side_menu_post_code_error));
            return;
        }
        CityController.getCityForPostCode(postCode).continueWith(new Continuation<String, Object>() {
            @Override
            public Object then(Task<String> task) throws Exception {
                if (task.getError() == null && task.isCompleted()) {
                    if (task.getResult() != null) {
                        save(username, postCode, task.getResult());
                    } else {
                        view.showError();
                    }
                } else {
                    view.showError();
                }
                return null;
            }
        });
    }

    @Override
    public void save(String userName, String postCode, String city) {
        UserController.storeUsername(userName);
        CityController.storeCity(city, postCode);
        view.showSuccess(userName, postCode, city);
    }

    private boolean checkField(String text) {
        return !TextUtils.isEmpty(text);
    }
}
