package aguila.ivan.sherpajob.demo.controllers;

import aguila.ivan.sherpajob.demo.model.Master;
import io.realm.Realm;

/**
 * Created by ivan on 17/7/17.
 */

public class UserController {

    public static void storeUsername(String username) {
        Master master = new Master();
        master.setUsername(username);
        try {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(master);
            realm.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
