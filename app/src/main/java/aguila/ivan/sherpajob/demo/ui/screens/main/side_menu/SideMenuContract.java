package aguila.ivan.sherpajob.demo.ui.screens.main.side_menu;

import aguila.ivan.sherpajob.demo.ui.base.BasePresenter;
import aguila.ivan.sherpajob.demo.ui.base.BaseView;

/**
 * Created by ivan on 17/7/17.
 */

public interface SideMenuContract {

    interface Presenter extends BasePresenter {

        void searchCityForPostCode(String username, String postCode);

        void save(String userName, String postCode, String city);

    }

    interface View extends BaseView<Presenter> {

        void showSuccess(String username, String city, String postCode);

        void showError();

    }

}
