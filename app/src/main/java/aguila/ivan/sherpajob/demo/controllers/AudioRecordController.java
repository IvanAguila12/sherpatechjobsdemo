package aguila.ivan.sherpajob.demo.controllers;

import aguila.ivan.sherpajob.demo.model.RecordedAudio;
import io.realm.Realm;
import io.realm.Sort;

/**
 * Created by ivan on 17/7/17.
 */

public class AudioRecordController {

    public static void storeHello(String text) {
        RecordedAudio audio = new RecordedAudio();
        audio.setAudio(text);
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(audio);
        realm.commitTransaction();
    }

    public static RecordedAudio getLastStoreAudio() {
        return Realm.getDefaultInstance().where(RecordedAudio.class).findAllSorted("id", Sort.DESCENDING).first();
    }

}
