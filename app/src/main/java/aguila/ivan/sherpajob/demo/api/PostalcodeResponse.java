package aguila.ivan.sherpajob.demo.api;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ivan on 17/7/17.
 */

public class PostalcodeResponse {

    private String placeName;
    @SerializedName("lat")
    private double latitude;
    @SerializedName("lng")
    private double longitude;

    public String getPlaceName() {
        return placeName;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
