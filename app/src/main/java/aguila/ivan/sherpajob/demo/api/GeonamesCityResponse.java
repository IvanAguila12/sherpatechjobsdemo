package aguila.ivan.sherpajob.demo.api;

import java.util.List;

/**
 * Created by ivan on 17/7/17.
 */

public class GeonamesCityResponse {

    private List<PostalcodeResponse> postalCodes;

    public List<PostalcodeResponse> getPostalCodes() {
        return postalCodes;
    }
}
