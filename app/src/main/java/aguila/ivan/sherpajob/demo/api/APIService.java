package aguila.ivan.sherpajob.demo.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import aguila.ivan.sherpajob.demo.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ivan on 17/7/17.
 */

public class APIService {

    private GeonamesService geonamesAPI = null;
    private Retrofit retrofit = null;
    private OkHttpClient httpClient = null;

    public static APIService getInstance() {
        return APIServiceHolder.INSTANCE;
    }

    private static class APIServiceHolder {
        private static final APIService INSTANCE = new APIService();
    }

    private APIService() {
        init();
    }

    private void init() {
        initOkHttp();
        initRetrofit();
        geonamesAPI = retrofit.create(GeonamesService.class);
    }

    private void initOkHttp() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        }

        builder.addInterceptor(loggingInterceptor);
        builder.connectTimeout(15, TimeUnit.SECONDS);
        builder.readTimeout(20, TimeUnit.SECONDS);
        builder.writeTimeout(20, TimeUnit.SECONDS);
        builder.retryOnConnectionFailure(true);
        httpClient = builder.build();
    }

    private void initRetrofit() {
        retrofit = new Retrofit.Builder()
                .baseUrl("http://api.geonames.org")
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(getGSON()))
                .build();
    }

    private Gson getGSON() {
        return new GsonBuilder()
                .create();
    }

    public GeonamesService getService() {
        return geonamesAPI;
    }

}
