package aguila.ivan.sherpajob.demo.ui.base;

/**
 * Created by ivan on 15/7/17.
 */

public interface BasePresenter {

    void start();

}
