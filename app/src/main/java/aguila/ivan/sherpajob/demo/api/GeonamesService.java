package aguila.ivan.sherpajob.demo.api;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by ivan on 17/7/17.
 */

public interface GeonamesService {

    String geoNamesPostCode = "/postalCodeSearchJSON";

    @GET(geoNamesPostCode)
    Call<GeonamesCityResponse> getCityForPostCode(@Query("postalcode") String postCode,
                                                  @Query("username") String username);

}
