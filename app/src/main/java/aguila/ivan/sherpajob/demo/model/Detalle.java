package aguila.ivan.sherpajob.demo.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ivan on 17/7/17.
 */

public class Detalle extends RealmObject {

    @PrimaryKey
    private String postCode;
    private String city;

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
