package aguila.ivan.sherpajob.demo.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;

/**
 * Created by ivan on 16/7/17.
 */
public class PermissionUtils {

    public static final int REQUEST_GROUP_LOCATION = 1111;
    public static final int REQUEST_RECORD_AUDIO = 1112;

    /**
     * Method to check if device has the permission
     *
     * @param context
     * @param permission
     * @return
     */
    public static boolean hasThisPermission(Context context, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return ActivityCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }

    /**
     * Request permission for activities
     *
     * @param activity
     * @param permission
     * @param requestCode
     * @return
     */
    public static boolean isPermissionRequestNeeded(Activity activity, String permission, int requestCode) {
        return isPermissionRequestNeeded(activity, null, permission, requestCode);
    }

    /**
     * Request permission for fragments
     *
     * @param activity
     * @param fragment
     * @param permission
     * @param requestCode
     * @return
     */
    public static boolean isPermissionRequestNeeded(Activity activity, Fragment fragment, String permission, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !hasThisPermission(activity, permission)) {
            final String[] permissions = new String[]{permission};
            if (fragment == null) {
                activity.requestPermissions(permissions, requestCode);
            } else {
                fragment.requestPermissions(permissions, requestCode);
            }
            return true;
        }
        return false;
    }
}
