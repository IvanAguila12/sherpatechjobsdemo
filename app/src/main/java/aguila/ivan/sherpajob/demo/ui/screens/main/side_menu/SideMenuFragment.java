package aguila.ivan.sherpajob.demo.ui.screens.main.side_menu;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import aguila.ivan.sherpajob.demo.R;
import aguila.ivan.sherpajob.demo.ui.base.BaseFragment;

/**
 * Created by ivan on 17/7/17.
 */

public class SideMenuFragment extends BaseFragment implements SideMenuContract.View, View.OnClickListener {

    private EditText userName, postCode;

    public static SideMenuFragment newInstance() {
        return new SideMenuFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_main_side_menu;
    }

    @Override
    protected void configView() {
        userName = $(R.id.side_menu_username);
        postCode = $(R.id.side_menu_post_code);

        $(R.id.side_menu_save).setOnClickListener(this);
    }

    @Override
    public void setPresenter(SideMenuContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.side_menu_save:
                getPresenter(SideMenuContract.Presenter.class).searchCityForPostCode(userName.getText().toString(), postCode.getText().toString());
                break;
        }
    }

    @Override
    public void showSuccess(String username, String postCode, String city) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getBaseActivity());
        builder.setMessage(getString(R.string.side_menu_save_success, username, city, postCode));
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void showError() {
        Toast.makeText(getBaseActivity(), "Error checking city", Toast.LENGTH_LONG).show();
    }
}
