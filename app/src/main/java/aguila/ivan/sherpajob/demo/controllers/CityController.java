package aguila.ivan.sherpajob.demo.controllers;

import android.location.Location;

import aguila.ivan.sherpajob.demo.api.APIService;
import aguila.ivan.sherpajob.demo.api.GeonamesCityResponse;
import aguila.ivan.sherpajob.demo.api.PostalcodeResponse;
import aguila.ivan.sherpajob.demo.model.Detalle;
import bolts.ExecutorException;
import bolts.Task;
import bolts.TaskCompletionSource;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ivan on 17/7/17.
 */

public class CityController {

    public static void storeCity(String city, String postCode) {
        Detalle detalle = new Detalle();
        detalle.setCity(city);
        detalle.setPostCode(postCode);
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(detalle);
        realm.commitTransaction();
    }

    public static Task<String> getCityForPostCode(String postCode) {
        final TaskCompletionSource<String> tcs = new TaskCompletionSource<>();
        APIService.getInstance().getService().getCityForPostCode(postCode, "iaguila").enqueue(new Callback<GeonamesCityResponse>() {
            @Override
            public void onResponse(Call<GeonamesCityResponse> call, Response<GeonamesCityResponse> response) {
                tcs.setResult(getClosestCityFromResponse(response.body()));
            }

            @Override
            public void onFailure(Call<GeonamesCityResponse> call, Throwable t) {
                tcs.setError(new Exception(t));
            }
        });
        return tcs.getTask();
    }

    private static String getClosestCityFromResponse(GeonamesCityResponse response) {
        if (response == null) return null;
        if (response.getPostalCodes() == null || response.getPostalCodes().isEmpty()) return null;
        if (LocationController.getInstance().getCurrentLocation() == null) return response.getPostalCodes().get(0).getPlaceName();
        float minDistance = Float.MAX_VALUE;
        String placeName = "";
        for (PostalcodeResponse postalcodeResponse : response.getPostalCodes()) {
            Location placeLocation = new Location("");
            placeLocation.setLatitude(postalcodeResponse.getLatitude());
            placeLocation.setLongitude(postalcodeResponse.getLongitude());
            float placeDistance = LocationController.getInstance().getCurrentLocation().distanceTo(placeLocation);
            if (placeDistance < minDistance) {
                minDistance = placeDistance;
                placeName = postalcodeResponse.getPlaceName();
            }
        }
        return placeName;
    }

}
