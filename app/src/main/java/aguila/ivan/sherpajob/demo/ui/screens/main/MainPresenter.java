package aguila.ivan.sherpajob.demo.ui.screens.main;

import android.location.Location;

import java.util.List;

import aguila.ivan.sherpajob.demo.R;
import aguila.ivan.sherpajob.demo.controllers.AudioRecordController;
import aguila.ivan.sherpajob.demo.controllers.LocationController;
import aguila.ivan.sherpajob.demo.ui.base.BaseActivity;
import bolts.Continuation;
import bolts.Task;

/**
 * Created by ivan on 16/7/17.
 */

public class MainPresenter implements MainContract.Presenter {

    private static final String RECORD_HELLO = "hola";
    private static final String RECORD_BYE = "adios";

    private BaseActivity activity;
    private MainContract.View view;

    public MainPresenter(BaseActivity activity, MainContract.View view) {
        this.activity = activity;
        this.view = view;
    }

    @Override
    public void start() {
        view.showUserLatitude(activity.getString(R.string.main_latitude));
        view.showUserLongitude(activity.getString(R.string.main_longiude));
    }

    @Override
    public void startUserLocation() {
        view.hideLocationError();
        LocationController.getInstance().getCurrentLocation(activity).continueWith(new Continuation<Location, Object>() {
            @Override
            public Object then(Task<Location> task) throws Exception {
                if (task.getError() == null && task.isCompleted()) {
                    view.showUserLatitude(activity.getString(R.string.main_latitude) + " " + task.getResult().getLatitude());
                    view.showUserLongitude(activity.getString(R.string.main_longiude) + " " + task.getResult().getLongitude());
                } else {
                    view.showLocationError();
                }
                return null;
            }
        });
    }

    @Override
    public void checkAudioRecorded(List<String> matches) {
        if (matches != null) {
            for (String match : matches) {
                if (match.contains(RECORD_HELLO)) {
                    storeInBBDD(match);
                    return;
                }
                if (match.contains(RECORD_BYE)) {
                    storeInPreferences(match);
                    return;
                }
            }
            view.showMicroRecordingError();
        }
    }

    @Override
    public void storeInBBDD(String text) {
        AudioRecordController.storeHello(text);
        view.showRecordedAudioText(activity.getString(R.string.main_stored_audio_bbdd, AudioRecordController.getLastStoreAudio().getAudio()));
    }

    @Override
    public void storeInPreferences(String text) {
        activity.getDemoApplication().storeString(RECORD_BYE, text);
        view.showRecordedAudioText(activity.getString(R.string.main_stored_audio_preferences, activity.getDemoApplication().getStoredString(RECORD_BYE, "")));
    }
}
