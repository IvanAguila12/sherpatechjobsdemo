package aguila.ivan.sherpajob.demo.ui.screens.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.AppCompatImageButton;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import aguila.ivan.sherpajob.demo.R;
import aguila.ivan.sherpajob.demo.ui.base.BaseActivity;
import aguila.ivan.sherpajob.demo.ui.screens.main.side_menu.SideMenuFragment;
import aguila.ivan.sherpajob.demo.ui.screens.main.side_menu.SideMenuPresenter;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    private DrawerLayout drawer;

    public static Intent makeIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void configView() {
        Toolbar toolbar = $(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = $(R.id.drawer_layout);

        $(R.id.main_menu_icon).setOnClickListener(this);

        MainFragment fragment = MainFragment.newInstance();
        fragment.setPresenter(new MainPresenter(this, fragment));
        loadFragment(fragment, R.id.main_content_frame);

        SideMenuFragment menuFragment = SideMenuFragment.newInstance();
        menuFragment.setPresenter(new SideMenuPresenter(this, menuFragment));
        loadFragment(menuFragment, R.id.main_menu_content_frame);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.main_menu_icon:
                if (drawer.isDrawerOpen(Gravity.END)) {
                    drawer.closeDrawer(Gravity.END);
                } else {
                    drawer.openDrawer(Gravity.END);
                }
                break;
        }
    }
}
