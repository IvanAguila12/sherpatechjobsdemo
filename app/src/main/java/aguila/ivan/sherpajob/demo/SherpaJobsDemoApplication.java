package aguila.ivan.sherpajob.demo;

import android.app.Application;
import android.content.SharedPreferences;
import android.util.Log;

import com.facebook.stetho.Stetho;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.log.RealmLog;

/**
 * Created by ivan on 15/7/17.
 */

public class SherpaJobsDemoApplication extends Application {

    private static final String PREFERENCES_NAME = "sherpaJobsDemo";

    private RealmConfiguration realmConfiguration;

    @Override
    public void onCreate() {
        super.onCreate();

        initRealm();
    }

    private void initRealm() {
        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                        .build());

        Realm.init(this);
        realmConfiguration = new RealmConfiguration.Builder()
                .schemaVersion(0)
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
        if (BuildConfig.DEBUG) {
            RealmLog.setLevel(Log.VERBOSE);
        }
    }
    private SharedPreferences getDemoPreferences() {
        return getSharedPreferences(PREFERENCES_NAME, MODE_PRIVATE);
    }

    public void storeBoolean(String key, boolean value) {
        getDemoPreferences().edit().putBoolean(key, value).apply();
    }

    public boolean checkBoolean(String key, boolean defaultValue) {
        return getDemoPreferences().getBoolean(key, defaultValue);
    }

    public void storeString(String key, String value) {
        getDemoPreferences().edit().putString(key, value).apply();
    }

    public String getStoredString(String key, String defaultValue) {
        return getDemoPreferences().getString(key, defaultValue);
    }

    public void storeInteger(String key, int value) {
        getDemoPreferences().edit().putInt(key, value).apply();
    }

    public int getStoredInteger(String key, int defaultValue) {
        return getDemoPreferences().getInt(key, defaultValue);
    }

    public void storeLong(String key, long value) {
        getDemoPreferences().edit().putLong(key, value).apply();
    }

    public long getStoredLong(String key, long defaultValue) {
        return getDemoPreferences().getLong(key, defaultValue);
    }

}
