package aguila.ivan.sherpajob.demo.ui.screens.splash;

import aguila.ivan.sherpajob.demo.ui.base.BasePresenter;
import aguila.ivan.sherpajob.demo.ui.base.BaseView;

/**
 * Created by ivan on 15/7/17.
 */

public interface SplashContract {

    interface Presenter extends BasePresenter {

    }

    interface View extends BaseView<Presenter> {

        void showText(String text);

        void animateText();

        void openApp();

    }

}
