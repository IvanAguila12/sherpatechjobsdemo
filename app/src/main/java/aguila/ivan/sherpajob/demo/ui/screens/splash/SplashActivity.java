package aguila.ivan.sherpajob.demo.ui.screens.splash;

import aguila.ivan.sherpajob.demo.ui.base.BaseActivity;

/**
 * Created by ivan on 15/7/17.
 */

public class SplashActivity extends BaseActivity {

    @Override
    protected void configView() {
        SplashFragment fragment = SplashFragment.newInstance();
        fragment.setPresenter(new SplashPresenter(this, fragment));
        loadFragment(fragment);
    }

    @Override
    protected int getLayoutResource() {
        return 0;
    }
}

