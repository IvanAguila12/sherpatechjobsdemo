package aguila.ivan.sherpajob.demo.model;

import java.util.Random;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ivan on 17/7/17.
 */

public class RecordedAudio extends RealmObject {

    @PrimaryKey
    private long id = System.currentTimeMillis();
    private String audio;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }
}
