package aguila.ivan.sherpajob.demo.ui.screens.splash;

import android.os.Handler;

import aguila.ivan.sherpajob.demo.R;
import aguila.ivan.sherpajob.demo.ui.base.BaseActivity;

/**
 * Created by ivan on 15/7/17.
 */

public class SplashPresenter implements SplashContract.Presenter {

    private BaseActivity activity;
    private SplashContract.View view;

    public SplashPresenter(BaseActivity activity, SplashContract.View view) {
        this.activity = activity;
        this.view = view;
    }

    @Override
    public void start() {
        view.showText(activity.getString(R.string.splash_text));

        Handler animateHandler = new Handler();
        animateHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                view.animateText();
            }
        }, 1000);

        Handler oppenAppHandler = new Handler();
        oppenAppHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                view.openApp();
            }
        }, 5000);
    }
}
