package aguila.ivan.sherpajob.demo.ui.base;


import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by ivan on 15/7/17.
 */

public abstract class BaseFragment extends Fragment {

    private View parentView;
    protected BasePresenter presenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(getLayoutResource(), container, false);

        return parentView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        configView();
        if (presenter != null)
            presenter.start();
    }

    protected <T extends View> T $(@IdRes int resId) {
        if (parentView != null)
            return (T) parentView.findViewById(resId);
        return null;
    }

    protected <T extends View> T $(@IdRes int resId, View parent) {
        if (parent != null)
            return (T) parent.findViewById(resId);
        return null;
    }

    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    public  <T extends BasePresenter> T getPresenter(Class<T> tClass) {
        return tClass.cast(presenter);
    }

    protected abstract
    @LayoutRes
    int getLayoutResource();

    protected abstract void configView();
}
