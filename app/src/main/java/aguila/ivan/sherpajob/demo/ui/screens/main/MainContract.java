package aguila.ivan.sherpajob.demo.ui.screens.main;

import java.util.List;

import aguila.ivan.sherpajob.demo.ui.base.BasePresenter;
import aguila.ivan.sherpajob.demo.ui.base.BaseView;

/**
 * Created by ivan on 16/7/17.
 */

public interface MainContract {

    interface Presenter extends BasePresenter {

        void startUserLocation();

        void checkAudioRecorded(List<String> matches);

        void storeInBBDD(String text);

        void storeInPreferences(String text);

    }

    interface View extends BaseView<Presenter> {

        void showUserLatitude(String latutide);

        void showUserLongitude(String longitude);

        void showLocationError();

        void hideLocationError();

        void openMicroRecording();

        void showRecordedAudioText(String text);

        void showMicroRecordingError();

    }

}
