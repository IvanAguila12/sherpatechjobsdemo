package aguila.ivan.sherpajob.demo.ui.base;

/**
 * Created by ivan on 15/7/17.
 */

public interface BaseView<T extends BasePresenter> {

    void setPresenter(T presenter);

}
