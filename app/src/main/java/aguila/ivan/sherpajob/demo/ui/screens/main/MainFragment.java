package aguila.ivan.sherpajob.demo.ui.screens.main;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import aguila.ivan.sherpajob.demo.R;
import aguila.ivan.sherpajob.demo.ui.base.BaseFragment;
import aguila.ivan.sherpajob.demo.utils.PermissionUtils;

/**
 * Created by ivan on 16/7/17.
 */

public class MainFragment extends BaseFragment implements MainContract.View, View.OnClickListener {

    private static final int REQUEST_AUDIO_RECORDING = 2222;

    private TextView latitude, longitude, locationError, recordSuccess, recordError;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_main;
    }

    @Override
    protected void configView() {
        latitude = $(R.id.main_location_latitude);
        longitude = $(R.id.main_location_longitude);
        locationError = $(R.id.main_location_error);
        recordSuccess = $(R.id.main_record_audio_success);
        recordError = $(R.id.main_record_audio_error);

        $(R.id.main_record).setOnClickListener(this);

        String location_permission = Manifest.permission.ACCESS_FINE_LOCATION;
        if (!PermissionUtils.isPermissionRequestNeeded(getBaseActivity(), this, location_permission, PermissionUtils.REQUEST_GROUP_LOCATION)) {
            checkLocationPermission();
        }
    }

    @Override
    public void setPresenter(MainContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showUserLatitude(String latutide) {
        this.latitude.setText(latutide);
    }

    @Override
    public void showUserLongitude(String longitude) {
        this.longitude.setText(longitude);
    }

    @Override
    public void showLocationError() {
        locationError.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLocationError() {
        locationError.setVisibility(View.INVISIBLE);
    }

    @Override
    public void openMicroRecording() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        // secret parameters that when added provide audio url in the result
        intent.putExtra("android.speech.extra.GET_AUDIO_FORMAT", "audio/AMR");
        intent.putExtra("android.speech.extra.GET_AUDIO", true);

        startActivityForResult(intent, REQUEST_AUDIO_RECORDING);
    }

    @Override
    public void showRecordedAudioText(String text) {
        recordSuccess.setText(text);
        recordSuccess.setVisibility(View.VISIBLE);
        recordError.setVisibility(View.GONE);
    }

    // handle result of speech recognition
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_AUDIO_RECORDING:
                // the resulting text is in the getExtras:
                if (resultCode == Activity.RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    ArrayList<String> matches = bundle.getStringArrayList(RecognizerIntent.EXTRA_RESULTS);
                    getPresenter(MainContract.Presenter.class).checkAudioRecorded(matches);
                }
                break;
        }
    }

    @Override
    public void showMicroRecordingError() {
        recordError.setText(getString(R.string.main_record_audio_error));
        recordError.setVisibility(View.VISIBLE);
        recordSuccess.setVisibility(View.GONE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PermissionUtils.REQUEST_GROUP_LOCATION:
                checkLocationPermission();
                break;
            case PermissionUtils.REQUEST_RECORD_AUDIO:
                checkRecordingPermission();
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void checkLocationPermission() {
        if (PermissionUtils.hasThisPermission(getBaseActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
            getPresenter(MainContract.Presenter.class).startUserLocation();
        } else {
            showLocationError();
        }
    }

    private void checkRecordingPermission() {
        if (PermissionUtils.hasThisPermission(getBaseActivity(), Manifest.permission.RECORD_AUDIO)) {
            openMicroRecording();
        } else {
            showMicroRecordingError();
        }
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.main_record:
                if (!PermissionUtils.isPermissionRequestNeeded(getBaseActivity(), this, Manifest.permission.RECORD_AUDIO, PermissionUtils.REQUEST_RECORD_AUDIO)) {
                    checkRecordingPermission();
                }
                break;
        }
    }
}
