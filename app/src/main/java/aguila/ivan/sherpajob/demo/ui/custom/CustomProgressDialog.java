package aguila.ivan.sherpajob.demo.ui.custom;

import android.app.Dialog;
import android.content.Context;

import aguila.ivan.sherpajob.demo.R;

public class CustomProgressDialog extends Dialog {

    public CustomProgressDialog(Context context, String error) {
        super(context, R.style.CustomProgressDialog);
        setCanceledOnTouchOutside(false);
        setCancelable(false);
    }


    @Override
    public void show() {
        super.show();
        setContentView(R.layout.view_progress_dialog);
    }

}
