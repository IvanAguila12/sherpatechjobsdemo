package aguila.ivan.sherpajob.demo.controllers;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import aguila.ivan.sherpajob.demo.ui.base.BaseActivity;
import bolts.Task;
import bolts.TaskCompletionSource;


/**
 * Created by ivan on 16/7/17.
 */

public class LocationController {

    private static LocationController ourInstance;

    private final int LOCATION_REFRESH = 60; //minutes
    private static final int MAX_WAIT_TIME_IN_MILLIS = 5000;
    private static final int NUM_UPDATES = 1;

    private GoogleApiClient googleApiClient;
    private LocationRequest mLocationRequest;

    private boolean located = false;

    private BaseActivity activity;

    private TaskCompletionSource<Location> tcs;

    private Location currentLocation;

    private LocationController() {

    }

    public static synchronized LocationController getInstance() {
        if (ourInstance == null)
            ourInstance = new LocationController();
        return ourInstance;
    }

    private GoogleApiClient.ConnectionCallbacks callbacks = new GoogleApiClient.ConnectionCallbacks() {
        @Override
        public void onConnected(@Nullable Bundle bundle) {
            startUserLocation();
        }

        @Override
        public void onConnectionSuspended(int i) {

        }
    };

    private GoogleApiClient.OnConnectionFailedListener failedListener = new GoogleApiClient.OnConnectionFailedListener() {
        @Override
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            if (tcs != null && tcs.getTask() != null && !tcs.getTask().isCompleted())
                tcs.setError(new Exception(connectionResult.getErrorMessage()));
        }
    };

    public Task<Location> getCurrentLocation(final BaseActivity activity) {
        this.activity = activity;
        if (tcs != null && tcs.getTask() != null && !tcs.getTask().isCompleted()) {
            tcs.setCancelled();
            tcs = null;
        }
        tcs = new TaskCompletionSource<>();

        located = false;
        initGoogleApiClient(activity);

        return tcs.getTask();
    }

    private void initGoogleApiClient(BaseActivity activity) {
        if (googleApiClient == null) {

            googleApiClient = new GoogleApiClient.Builder(activity)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(callbacks)
                    .addOnConnectionFailedListener(failedListener)
                    .build();

            googleApiClient.connect();
        } else if (googleApiClient.isConnected()){
            startUserLocation();
        } else {
            googleApiClient.connect();
        }
    }

    private void startUserLocation() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setMaxWaitTime(MAX_WAIT_TIME_IN_MILLIS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setNumUpdates(NUM_UPDATES);
        try {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, mLocationRequest, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        located = true;
                        currentLocation = location;
                        if (tcs != null && tcs.getTask() != null && !tcs.getTask().isCompleted())
                            tcs.setResult(location);
                    } else {
                        if (tcs != null && tcs.getTask() != null && !tcs.getTask().isCompleted())
                            tcs.setResult(null);
                    }
                }
            });
        } catch (SecurityException e) {
            e.printStackTrace();
        }
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!located) {
                    if (tcs != null && tcs.getTask() != null && !tcs.getTask().isCompleted())
                        tcs.setError(null);
                }
            }
        }, MAX_WAIT_TIME_IN_MILLIS);
    }

    public Location getCurrentLocation() {
        return currentLocation;
    }
}
