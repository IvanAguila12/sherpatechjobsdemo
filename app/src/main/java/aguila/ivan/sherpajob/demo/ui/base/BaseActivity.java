package aguila.ivan.sherpajob.demo.ui.base;

import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import aguila.ivan.sherpajob.demo.R;
import aguila.ivan.sherpajob.demo.SherpaJobsDemoApplication;
import aguila.ivan.sherpajob.demo.interfaces.PermissionInterface;
import aguila.ivan.sherpajob.demo.ui.custom.CustomProgressDialog;

/**
 * Created by ivan on 15/7/17.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private CustomProgressDialog progressDialog;
    protected BaseFragment currentFragment;
    private PermissionInterface permissionInterface;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getLayoutResource() != 0)
            setContentView(getLayoutResource());
        else
            setContentView(R.layout.activity_base_layout);

        configView();
    }

    protected void loadFragment(final BaseFragment fragment) {
        loadFragment(fragment, R.id.content_frame);
    }

    public void loadFragment(final BaseFragment fragment, int frameId) {
        try {
            currentFragment = fragment;
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(frameId, fragment);
            transaction.commitAllowingStateLoss();
        } catch (IllegalStateException e) {

        }
    }

    protected void hideFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.hide(fragment);
        transaction.commit();
    }

    public void showProgressDialog() {
        if (progressDialog == null)
            progressDialog = new CustomProgressDialog(this, null);
        progressDialog.show();
    }

    public void dismissProgressDialog() {
        dismissProgressDialog();
    }

    public void showErrorMessage(String error) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(error);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    protected abstract void configView();

    protected abstract int getLayoutResource();

    protected <T extends View> T $(int viewId) {
        return (T) findViewById(viewId);
    }

    protected <T extends View> T $(int viewId, View parent) {
        return (T) parent.findViewById(viewId);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        requestCodePermission(requestCode, grantResults[0] == PackageManager.PERMISSION_GRANTED);
    }

    private void requestCodePermission(int requestCode, boolean permissionGranted) {
        if (permissionInterface == null)
            return;
        permissionInterface.onPermissionRequestResult(requestCode, permissionGranted);
    }

    public PermissionInterface getPermissionInterface() {
        return permissionInterface;
    }

    public void setPermissionInterface(PermissionInterface permissionInterface) {
        this.permissionInterface = permissionInterface;
    }

    public SherpaJobsDemoApplication getDemoApplication() {
        return (SherpaJobsDemoApplication) getApplication();
    }


}
