package aguila.ivan.sherpajob.demo.interfaces;

/**
 * Created by ivan on 15/7/17.
 */
public interface PermissionInterface {
    void onPermissionRequestResult(int requestCode, boolean permissionGranted);
}